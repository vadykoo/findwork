<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Job;
use App\models\Skill;
use App\Models\User;
use App\Models\UserSkill;
use App\Traits\FormatResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Notifications\MyNotification;

class UserController extends Controller
{
  //  use FormatResponse;
    public function my_profile()
    {
        $user = Auth::guard('api')->user()->only('last_name', 'phone', 'first_name', 'email', 'city', 'passport_id', 'photo_id', 'photo_path');
//        $data = array(
//            'profile' => $user->only('last_name', 'phone', 'first_name', 'email', 'city', 'passport_id', 'photo_id'),
//            'photo_path' => self::profile_photo_path($user)
//        );
        return response()->json($user, 200);
    }



    public function resetpass() {

    }

    public function sendcode() {
      
    }


     public function recovery(Requests $request) {

          $user = User::where('phone', substr($request->phone, -9))->first();

        

          if (!empty($user)) {

          $arr = ['api_id' => '3BD9ECC2-46D6-7AEC-96E0-9DF8E8F7A1C7', 'phone' => '380'.$user->phone, 'msg' => 'test code:'];

            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', 'https://sms.ru/sms/send?api_id='.$arr['api_id'].'&to='.$arr['phone'].'&msg='.$arr['msg'].'&json=1');
            echo $res->getStatusCode();
          }
          // https://sms.ru/sms/send?api_id=3BD9ECC2-46D6-7AEC-96E0-9DF8E8F7A1C7&to=380988768865,74993221627&msg=hello+world&json=1

     }

//    public function profile_photo_path($user) {
////        $photo_path = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix('profile_photo/'.$user->photo_id.'.png');
//        $filename = $user->photo_id.'.png';
////        $path = asset( $filename);
//        if ($user->photo_id == NULL){
//            return NULL;
//        }
//        else {
//            return 'http://hire-man.grassbusinesslabs.tk/storage/' . $filename;
//        }
//    }

    public function base64image(UserUpdateRequest $request){
        $image = $request->photo; // your base64 encoded data:image/png;base64,
        $rand_name = rand(1111, 9999);
        $image = str_replace('data:image/*;charset=utf-8;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
//        Storage::disk('local')->put($rand_name.'.txt', $image);
        if(is_array($image)) {
            $image = implode($image);
        }
        $imageName = $rand_name . '.png';
//            \File::put(storage_path(). '/' . $imageName, base64_decode($image));
//        $filename = 'profile_photo/'.$user->photo_id.'.png';
//        $path = asset( $filename);
        Storage::disk('public')->put($imageName, base64_decode($image));
//        base64_decode($image)->move(public_path("/uploads"), $newfilename)
//        $request->photo = $imageName;
        return $rand_name;


    }


    public function updateProfile(UserUpdateRequest $request)
    {
            $user = Auth::guard('api')->user();
//            $aboutMe = User::where('id', $user->id)->first();
            if (!empty($request->first_name)) $user->first_name = $request->first_name;
            if (!empty($request->last_name)) $user->last_name = $request->last_name;
            if (!empty($request->phone)) $user->phone = $request->phone;
            if (!empty($request->email)) $user->email = $request->email;
            if (!empty($request->city)) $user->city = $request->city;
            if (!empty($request->passport_id)) $user->passport_id = $request->passport_id;
//            if (!empty($request->first_name_ENG)) $aboutMe->first_name_ENG = $request->first_name_ENG;
//            if (!empty($request->last_name_ENG)) $aboutMe->last_name_ENG = $request->last_name_ENG;
            if(!empty($request->photo)) {
                $user->photo_id = self::base64image($request);
            }
            if (!empty($request))
                $user->save();
        return response()->json($user, 200);
    }


    public function tape() {
        $user = Auth::guard('api')->user();
        $ss = UserSkill::select('skill_id','lvl')->where('user_id', $user->id)->get();

        $count= count($ss->all());

        $x = 0;
        while ($x < $count) {
            $result = JobSkill::where('skill_id', '=', $ss[$x]['skill_id'])
                ->where('lvl', '<', $ss[$x]['lvl'])
                ->OrWhere('lvl', '=', $ss[$x]['lvl'])->select('job_id')
                ->first();

            if ($result != NULL) $mass[] = $result;

            $res[] = $mass[$x]['job_id'];

            $x++;
        }

        $result = Job::WhereIn('id', $res)->get();

        return response()->json($result, 201);
    }

    public function updatePassword(Request $request)
    {
        $user = Auth::guard('api')->user();
        $member = User::where('id', $user->id)->first();

        if (Hash::check($request->old_password, $member->password)) {
                $member->password = bcrypt($request->new_password);
                $member->save();
                return response()->json("Good Reset Password", 200);
        } else {
            return response()->json("Not Reset", 400);
        }

    }
//    public function pushed(){
//$push = new MyNotification();
//$push->push();
//        return response()->json('Отправлено', 201);
//    }


}
