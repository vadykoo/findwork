<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Auth::routes();

Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\RegisterController@register');

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('user', 'UserController');

    //support
    Route::apiResource('support' , 'SupportController');
    Route::get('mysupport' ,  'SupportController@mysupport');
    //skill
    Route::apiResource('skill' , 'SkillController');
    Route::put('skill/{id}', 'SkillController@update');
    Route::get('myskill', 'SkillController@myskill');
    Route::get('destroyskill/{id}', 'SkillController@destroyskill');
    Route::post('addskill', 'SkillController@addSkill');
    Route::get('destroyskill/{id}', 'SkillController@destroyskill');
    Route::post('deleteskill','SkillController@kick');
    Route::post('updatelevel/{userSkill}', 'SkillController@updatelevel');
//job
    \
    Route::apiResource('jobs', 'JobController');
    Route::get('see_job/{id}', 'JobController@seeone');
    Route::post('search', 'JobController@search');
    Route::post('addorder', 'JobController@order');
    Route::get('myjobs', 'JobController@myjobs');
    Route::get('deleteorder/{id}', 'JobController@kickorder');
    Route::get('orderjobs', 'JobController@orderjobs');
    //user
    ///// User
    Route::post('updatePassword', 'UserController@updatePassword');
    Route::post('update_me', 'UserController@updateProfile');//creating and updating user
    Route::get('my_profile', 'UserController@my_profile');
    // User -> tape
    Route::get('tape', 'UserController@tape');

    Route::post('reset_password' , 'UserController@updatePassword');
    Route::post('updatelevel/{userSkill}', 'SkillController@updatelevel');
//    friends
    Route::get('friend', 'FriendController@myfriends');
    Route::post('friend/send', 'FriendController@sendFriendRequest');
    Route::get('friend/{user}/accept', 'FriendController@acceptFriendRequest');
    Route::get('friend/{user}/deny', 'FriendController@denyFriendRequest');
    Route::get('friend/{user}/delete', 'FriendController@destroyFriend');

//    Route::get('friend/{user}/delete', 'FriendController@destroyFriend');
    Route::post('friend/delete', 'FriendController@destroyFriend');

    Route::get('friend/incoming', 'FriendController@incomingfriends');
    Route::get('friend/any', 'FriendController@anyfriends');


    Route::post('share','FriendController@addShare');

    Route::post('base64', 'AboutMeController@base64image');

    Route::get('project/{projectid}/send', 'ProjectApplicationController@sendProjectRequest');


});
