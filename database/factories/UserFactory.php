<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstNameMale,
        'last_name' => $faker->lastName,
        'phone' => rand ( 000000000,999999999),
        'email' => $faker->safeEmail,
        'city' => str_random(10),
        'passport_id' => 0,
        'photo_id' => 0,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
        'api_token' => str_random(10),
        'remember_token' => str_random(10),
    ];
});

