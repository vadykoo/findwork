<?php

use App\models\Skill;
use App\Models\User;
use Faker\Generator as Faker;



$factory->define(\App\Models\UserSkill::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomElement(User::pluck('id')->toArray()),
        'skill_id' => $faker->randomElement(Skill::pluck('id')->toArray()),
        'lvl' => rand(1,10),
    ];
});

