<?php

namespace App\Http\Controllers;

use App\models\OrderUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class FriendController extends Controller
{

    public function myfriends()
    {
        $user = Auth::user();
        $friends = $user->friends();
        return response()->json($friends, 200);

    }

    public function sendFriendRequest (Request $request)
    {
        $us = Auth::guard('api')->user();
         
         foreach ($request->phone as $key => $value) {
         	
                $gold[] = substr($value, -9);

         }

        
        $user = DB::table('users')->select('id')->whereIn('phone' , $gold)->get();


        foreach ($user as $u) {

            $empt = DB::table('friends')->where('sender_id' , $us->id)->where('recipient_id', $u->id)->get();

            if(!empty($empt) == false) {
                DB::table('friends')
                    ->insert(
                        ['sender_id' => $us->id, 'recipient_id' => $u->id, 'status' => 0]
                    );
            } else {
            	return 'no add';
            }

        }

        return $user;
    }

    public function acceptFriendRequest ($sender)
    {
        $user = Auth::user();
        $user->acceptFriendRequestFrom($sender);
        return response()->json($user, 200);
    }

    public function denyFriendRequest ($sender)
    {
        $user = Auth::user();
        $user->denyFriendRequestFrom($sender);
        return response()->json($user, 200);
    }

    public function destroyFriend ($douchebag)
    {
        $user = Auth::user();
        $user->deleteFriend($douchebag);
        return response()->json($user, 200);
    }

    public function incomingfriends ()
    {
        $user = Auth::user();
        $friends = $user->incoming_friends();
        return response()->json($friends, 200);
    }

    public function anyfriends ()
    {
        $user = Auth::user();
        $friends = $user->any_friends();
        return response()->json($friends, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($recipient)
    {
//
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addShare(Request $request)
    {
        {

            $user = Auth::guard('api')->user();

            $count = count($request->all());

            $x = 0;

            while ($x < $count) {

                $pro = OrderUser::where('recipient_id', $request[$x]['recipient_id'])->where('sender_id', $user->id)->first();
                if (!empty($pro) == false) {
                    OrderUser::insert(
                        array('sender_id' => $user->id, 'recipient_id' => $request[$x]['recipient_id'], 'job_id' => $request[$x]['job_id'])
                    );
                } else {
                    return response()->json("Error:" . $request[$x]['recipient_id'], 201);
                }
                $x++;
            }
            return response()->json($count, 201);


        }
    }
}