<?php

namespace App\Http\Controllers;

use App\Models\OrderUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class FriendController extends Controller
{
    public function myfriends()
    {
        $user = Auth::user();
        $friends = $user->friends();
        return response()->json($friends, 200);
    }

    public function sendFriendRequest (Request $request)
    {
        $us = Auth::guard('api')->user();
        $i=0;
        foreach ($request->phone as $number) {
            $p = substr($number, -9);
            $user = DB::table('users')->select('id')->where('phone', $p)->first();
            if(!empty($user) ) {
                $us->sendFriendRequestTo($user->id);
                $ret[$i++] = [$p,  'Added'];
            }
            else {
                    $textsms = 'Користувач пропонує вам встановити додаток HireMan';
                    $ret[$i++] = [$p, User::sendsms($p, $textsms)];
            }
        }
        return response()->json($ret, 200);
    }

    public function acceptFriendRequest($sender)
    {
        $user = Auth::user();
        $user->acceptFriendRequestFrom($sender);
        return response()->json($user, 200);
    }

    public function denyFriendRequest($sender)
    {
        $user = Auth::user();
        $user->denyFriendRequestFrom($sender);
        return response()->json($user, 200);
    }

    public function destroyFriend (Request $request)
    {
        $user = Auth::user();
        $douchebag = $request->user_id;
        foreach ($douchebag as $u) {
            $user->deleteFriend($u);
        }
        return response()->json($user, 200);
    }

    public function incomingfriends()
    {
        $user = Auth::user();
        $friends = $user->incoming_friends();
        return response()->json($friends, 200);
    }

    public function anyfriends()
    {
        $user = Auth::user();
        $friends = $user->any_friends();
        return response()->json($friends, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function addShare(Request $request)
    {
        {

            $user = Auth::guard('api')->user();



            $count = count($request['recipient_id']);

            $x = 0;

            foreach($request['recipient_id'] as $resID) {


                foreach ($request['job_id'] as $jobId) {
                    $pro = OrderUser::where('recipient_id', $request['recipient_id'][$x])->where('sender_id', $user->id)->where('job_id', $jobId)->first();
                    if (!empty($pro) == false) {
                        OrderUser::insert(
                            array('sender_id' => $user->id, 'recipient_id' => $request['recipient_id'][$x], 'job_id' => $jobId)
                        );
                    }
//                    else {
////                        return ['Error' => $request['recipient_id'][$x]];
////                            ("Error:" . $request['recipient_id'][$x], 400);
//                    }
                }

                $x++;
            }
            return response()->json($count, 201);
        }
    }


}