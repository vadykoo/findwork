<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobRequest;
use App\Models\Job;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use App\Models\OrderUser;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{


   public function index() {

        $all = Job::resTime(true);
      
        return response()->json($all, 200);
    }

    //Подача заявки
    public function store(JobRequest $request)
    {
        $user = Auth::guard('api')->user();

        $uJob = Order::where([
            ['user_id', '=', $user->id],
            ['job_id', '=', $request->job_id],])->first();
        if(!empty($uJob) == false) {
            $data = array(
                'user_id' => $user->id,
                'job_id' => $request->job_id,
            );

            $jobs = new Order();
            $jobs->create($data);

            return response()->json($data, 201);
        }
        return response()->json("Error", 201);

    }

    //Просмотр работы (счетчик)
    public function show($id)
    {
        $seeProject = Job::where('id', $id)->first();

        if(!empty($_COOKIE['count'])) {
            $cookcount = explode(".", $_COOKIE['count']);

            $i = 0;
            foreach($cookcount as $it) {
                if($it[$i] == $id) $true = true;
                $i++;
            }

            if($id != true) {
                $addsee = $seeProject->count + 1;
                $seeProject->count = $addsee;
                $seeProject->save();
                Cookie::forever('count', $id);
            }

        } else {
            $addsee = $seeProject->count + 1;
            $seeProject->count = $addsee;
            $seeProject->save();
            setcookie('count', $id);
        }

        return response()->json($seeProject, 200);
    }

    //поиск
    public function search(Request $request) {

        if(!empty($request->search)) {
            $allsearch = Job::where('title', 'like', '%' . $request->search . '%')->get();
            return response()->json($allsearch, 200);
        } else {
            return response()->json("Search field is empty!", 400);
        }
    }

    //удаление заяки
    public function destroy($id) {
        $select = Order::where('id', $id)->first();
        if(!empty($select)) {
            $order = Order::where('id', $id)->delete();
            return response()->json($order, 201);
        } else {
            return response()->json("record not found", 201);
        }
    }


    public function myjobs() {

        $all = Job::resTime(false);

        return response()->json($all, 200);

    }




    public function orderjobs() {
        $user = Auth::guard('api')->user();

       $ord = OrderUser::where('recipient_id', $user->id)->get(['sender_id']);

        $arr_tags = DB::table('jobs')

            ->join('order_users', 'job_id', '=', 'jobs.id')
            ->where('recipient_id', $user->id)
            ->select('order_users.recipient_id', 'order_users.job_id','order_users.sender_id','jobs.id','jobs.title','jobs.start_date','jobs.end_date',
                'jobs.price','jobs.city','jobs.description','jobs.food','jobs.residence','jobs.transportation','jobs.count','jobs.created_at','jobs.updated_at')
            ->where('recipient_id', $user->id)
            //    ->orderBy('id', 'desc')
            ->get();


        foreach ($ord as $key => $value) {
            $ordUser[] = User::select('first_name','last_name','photo_id')->whereIn('id', $value)->first();
        }

        $i = 0;
        foreach ($arr_tags as $oUs) {
            $oUs = collect($oUs);
            $oUs->put('first_name', $ordUser[$i]['first_name']);
            $oUs->put('last_name', $ordUser[$i]['last_name']);
            $oUs->put('photo_path', 'http://'.$_SERVER['HTTP_HOST'].'/storage/'.$ordUser[$i]['photo_id'].'.png');
            $oUs->all();
            $all[] = $oUs;
            $i++;
        }
        if (empty($all)) $all = [];

        return $all;
    }

}