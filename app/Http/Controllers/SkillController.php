<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSkillRequest;
use App\models\Skill;
use App\Http\Requests\SkillRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\UserSkill;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class SkillController extends Controller
{

    public function index()
    {
        $user = Auth::guard('api')->user();
        $userSkills = UserSkill::select('skill_id')->where('user_id', $user->id)->get();

        $result['all'] = Skill::whereNotIn('id', $userSkills)->get();
        $result['myskill'] = Skill::whereIn('id', $userSkills)->get();


        return $result;
    }


    public function myskill()
    {
        $user = Auth::guard('api')->user();
        $uSkill = UserSkill::where('user_id', $user->id)->select('skill_id', 'lvl')->get();
        $uLVL = UserSkill::where('user_id', $user->id)->select('lvl')->get();
        $arr_tags = DB::table('skills')
            ->join('user_skills', 'skill_id', '=', 'skills.id')
            ->where('user_id', $user->id)
            ->select('user_skills.id', 'skills.description', 'user_skills.lvl')
            ->get();
        return response()->json($arr_tags, 200);
    }

    public function show(Skill $skill)
    {
        return $skill;
    }

    public function store(Request $request)

    {

            $user = Auth::guard('api')->user();

            $count = count($request->all());

            $x = 0;

            while ($x < $count) {

                $pro = UserSkill::where('skill_id', $request[$x]['skill_id'])->where('user_id', $user->id)->first();
                if (!empty($pro) == false) {
                    UserSkill::insert(
                        array('user_id' => $user->id, 'skill_id' => $request[$x]['skill_id'], 'lvl' => $request[$x]['level'])
                    );
                } else {
                    return response()->json('Error', 201);
                }
                $x++;
            }
            return response()->json($count, 201);


    }

    public function update(SkillRequest $request, Skill $skill)
    {
        $skill->update($request->all());

        return response()->json($skill, 200);
    }

    public function destroy(Skill $skill)
    {
        $skill->delete();

        return response()->json(null, 204);
    }

    public function kick(Request $request)
    {
        UserSkill::whereIn('id', $request['skill_id'])->delete();
        return response()->json($request['skill_id'], 201);
    }



    public function updatelevel(Request $request)
    {
        $credentials = $request->all();
        $user = Auth::guard('api')->user();
        foreach ($credentials as $item) {
            $user_skill = UserSkill::where('id', $item['id'])->first();
            if ($user->id != $user_skill->user_id) {
            return response()->json(['success' => false, 'error' => 'id is incorrect or it`s not your skills']);
        }
            $user_skill->lvl = $item['lvl'];
            $user_skill->save();
        }
        return [
            'status' => true
        ];
    }
}

