<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Job;
use App\models\Skill;
use App\Models\User;
use App\Models\UserSkill;
//use App\Traits\FormatResponse;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
  //  use FormatResponse;
    public function index()
    {
        $user = Auth::guard('api')->user()->only('last_name', 'phone', 'first_name', 'email', 'city', 'passport_id', 'photo_id', 'photo_path');
        $app = app();
        $user_object = $app->make('stdClass');
        $user_object->profile = $user;
        return response()->json($user_object, 200);
    }



    public function update(UserUpdateRequest $request)
    {
            $user = Auth::guard('api')->user();
            if (!empty($request->first_name)) $user->first_name = $request->first_name;
            if (!empty($request->last_name)) $user->last_name = $request->last_name;
            if (!empty($request->phone)) $user->phone = $request->phone;
            if (!empty($request->email)) $user->email = $request->email;
            if (!empty($request->city)) $user->city = $request->city;
            if (!empty($request->passport_id)) $user->passport_id = $request->passport_id;
            if (!empty($request->photo)) {
                $user->photo_id = User::base64image($request);
            }
            if (!empty($request))
                $user->save();
        return response()->json($user, 200);
    }


    public function tape() {
        $user = Auth::guard('api')->user();
        $ss = UserSkill::select('skill_id','lvl')->where('user_id', $user->id)->get();

        $count= count($ss->all());

        $x = 0;
        while ($x < $count) {
            $result = JobSkill::where('skill_id', '=', $ss[$x]['skill_id'])
                ->where('lvl', '<', $ss[$x]['lvl'])
                ->OrWhere('lvl', '=', $ss[$x]['lvl'])->select('job_id')
                ->first();

            if ($result != NULL) $mass[] = $result;

            $res[] = $mass[$x]['job_id'];

            $x++;
        }

        $result = Job::WhereIn('id', $res)->get();

        return response()->json($result, 201);
    }
    public function pushh() {

    }
}
