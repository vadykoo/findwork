<?php

namespace App\Http\Controllers;

use App\Models\Support;
use App\Traits\FormatResponse;
use Illuminate\Http\Request;
use App\Http\Requests\SupportRequest;
use Illuminate\Support\Facades\Auth;
class SupportController extends Controller
{
use FormatResponse;
    public function __construct()
    {
        $this->authorizeResource(Support::class);
    }

    public function index()
    {
        $this->authorize('index', Support::class);
        return Support::all();
    }


    public function show(Support $support)
    {
        return $support;
    }

    public function store(SupportRequest $request)
    {
        $support = Support::create($request->all());

        $response = $this->formatResponse('successful',null ,$support);
        return response($response, 200);
    }

    public function update(SupportRequest $request, Support $support)
    {
        $support->update($request->all());

        return response()->json($support, 200);
    }

    public function destroy(Support $support)
    {
        $support->delete();

        return response()->json(null, 204);
    }
    public function mysupport() {
        $user = Auth::guard('api')->user();
        $skill = Support::where('created_by', $user->id)->get();
        return response()->json($skill, 201);
    }
}
