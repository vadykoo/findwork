<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Facades\FCM;
use App\Models\Device;
use Illuminate\Support\Facades\Log;

class PushController extends Controller
{
    /**
     * Send notification through FCM
     *
     * @param string|array $to
     * @param array $notification
     * @param array $data
     * @param array $options
     * @throws \LaravelFCM\Message\Exceptions\InvalidOptionsException
     * @throws \LaravelFCM\Message\InvalidOptionException
     */
    public static function sendMessage()
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder('my title');
        $notificationBuilder->setBody('Hello world')
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = "ckFYulRbP-E:APA91bEE0JvxXJU84YNrnnx4pMuPZztNA4u3ckdQRJsV7_StMc6U9AT38WfcMlpYdkyJJYQc2mNzDqepFvIEiAkX1VZV7foQtru0SEoDoB-08j4IRbiWDCrBfA2oKfSoyvgL1RWb9K5r";

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        $errors = $downstreamResponse->tokensWithError();
        if ($errors) {
            Log::channel('fcmlog')->info('Tokens with error: '.json_encode($errors));
        }
//return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

//return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

//return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

// return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }
}
