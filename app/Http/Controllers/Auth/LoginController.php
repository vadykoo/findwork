<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use App\Http\Requests\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{
    /*
   |--------------------------------------------------------------------------
   | Login Controller
   |--------------------------------------------------------------------------
   |
   | This controller handles authenticating users for the application and
   | redirecting them to your home screen. The controller uses a trait
   | to conveniently provide its functionality to your applications.
   |
   */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request, User $user)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $user = $this->guard()->user();
            $user->generateToken(true);

            if (!empty($request->device_token)) {  $device = $request->device_token; } else {
                $device = 'no';
            }

            $user->update([
                'device_token' => $device
            ]);

            return [
                'status'=>true,
                'user'=>$user
            ];
        }


        $user->update([
            'api_token' => $user->api_token,
            'device_token' => $request->device_token
        ]);


        return [
            'status'=>false,
            'msg' => $this->sendFailedLoginResponse($request)
        ];

    }

    public function logout(User $user)
    {
//        $user = User::find(1);

        $user = Auth::guard('api')->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return ['message' => 'User logged out.'];

    }
}