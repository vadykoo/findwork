<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    //востановление пароля
    public function resetpass(UserUpdateRequest $request) {
        $user = User::where('remember_token', $request->code)->first();

        if ($user AND !password_verify( $request->password , $user->password )) {
            $user->password = bcrypt($request->password);
            $user->remember_token = NULL;
            $user->save();
            return response()->json("Пароль змінено", 200);
        } else {
            return response()->json("Пароль не змінено, можливо такйи пароль вже використовувався.", 400);
        }
    }

    public function sendcode(UserUpdateRequest $request) {
        $user = User::where('remember_token', $request->code)->first();
        if ($user) {
            return response()->json("Good sms token", 200);
        } else {
            return response()->json("Bad sms token", 400);
        }
    }

//        public function sendsms($phone, $textsms) {
//        $arr = ['login' => 'hireamansms', 'password' => '5MS42W1', 'phone' => '+380'.$phone, 'msg' => iconv('utf-8','windows-1251',$textsms)];
//            dd('=)');
//        $client = new \GuzzleHttp\Client();
//        $res = $client->request('POST', 'https://smsc.ua/sys/send.php?login='.$arr['login'].'&psw='.$arr['password'].'&phones=+'.$arr['phone'].'&mes='.$arr['msg']);
//        return $res->getStatusCode();
//    }

    public function recovery(UserUpdateRequest $request) {
        $user = User::where('phone', substr($request->phone, -9))->first();
        if (!empty($user)) {
            $token = bin2hex(random_bytes(2));
            $user->remember_token = $token;
            $user->save();
            $textsms = 'Ваш код для зміни паролю: '.$token ;
            return User::sendsms($user->phone, $textsms);
        }
        else{
            return response()->json('Користувача не існує', 400);
        }
    }
//востановление пароля
}
