<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'first_name' => 'required|string|max:255',
            //'last_name' => 'required|string|max:255',
            // 'email' => 'required|string|email|max:255|unique:members',
            'password' => 'required|string|min:6',
            'phone' => 'required|integer|digits:9|'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            //'first_name' => $data['first_name'],
            //'last_name' => $data['last_name'],
            //'email' => $data['email'],
            'password' => bcrypt($data['password']),
//            'phone' => array_key_exists('phone', $data) ? $data['phone'] : null,
            'phone' => $data['phone'] ,
            'api_token' => User::generateToken(),
            'role_id' => 0, // 0 - employer
        ]);
    }

    protected function registered($request, $user)
    {
        return response()->json(['data' => $user->toArray()], 201);
    }
}
