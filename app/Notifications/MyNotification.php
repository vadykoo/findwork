<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\User;
class MyNotification extends Notification
{
    use Queueable;

    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function toFcm($notifiable)
    {

        return (new FirebaseMessage())->setContent('Test Notification', 'This is a Test');

    }
    public function push(){
        $User = User::find(1);

//Send Notification
        $User->notify(new MyNotification());
    }


}