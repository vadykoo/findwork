<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $response = parent::convertValidationExceptionToResponse($e, $request);
        if ($response instanceof JsonResponse) {
            $original = $response->getOriginalContent();
//            dd($original['errors']['phone'][0]);
            if(isset($original['errors']['phone']))
            {$original['message'] = __($original['errors']['phone'][0]);
            };
            if(isset($original['errors']['password']))
            {
                $original['message'] = __($original['errors']['password'][0]);
            }
//            dd(json_encode(__($original['errors']['password'][0])));
//            $original['errorUA'] = __($original['errors']['phone'][0]) + __($original['errors']['password'][0]);
//            dd(json_encode($original));
            $response->setContent(json_encode($original));
        }
        return $response;
    }
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
}
