<?php

namespace App\Models;
use App\Traits\Sms;
use Illuminate\Support\Facades\Storage;
//use Arubacao\Friends\Traits\Friendable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Friendable;
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use Friendable;
    use Sms;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'city',
        'passport_id',
        'photo_id',
        'password',
        'api_token',
        'role_id',
        'device_token',
    ];

    protected $appends = ['role', 'photo_path'];
    public function getPhotoPathAttribute()
    {
        $filename = $this->photo_id.'.png';
        if ($this->photo_id == NULL){
            return NULL;
        }
        else {
            return $this->path = 'http://hire-man.grassbusinesslabs.tk/storage/' . $filename;
        }
    }

    public function user(){
        return $this->hasMany(UserSkill::class);
    }
    public function order(){
        return $this->hasMany(Order::class);
    }

    public function orderUser(){
        return $this->hasMany(OrderUser::class);
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */


    public static function generateToken()
    {
        $token = str_random(60);
        if (User::where('api_token', $token)->first()) {
            return self::generateToken();
        }
        return $token;
    }

    protected $roles = [
        'worker',         // => 0
        'administrator',     // => 1
    ];

    /**
     * Get string representation of role_id
     * @return string
     */
    public function getRoleAttribute()
    {
        return Arr::get($this->roles, $this->role_id);
    }
    public static function base64image($request){
        $image = $request->photo; // your base64 encoded data:image/png;base64,
        $rand_name = rand(1111, 9999);
        $image = str_replace('data:image/*;charset=utf-8;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        if(is_array($image)) {
            $image = implode($image);
        }
        $imageName = $rand_name . '.png';
        Storage::disk('public')->put($imageName, base64_decode($image));
        return $rand_name;
    }
}
