<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use App\Traits\Managed;
use Illuminate\Support\Facades\Storage;
class Order extends Model
{
    use Managed;

    protected $fillable = ['job_id', 'user_id','status_id'];

    public function job(){
        return $this->belongsTo(Job::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public static function boot() {
        //parent::boot();
       // self::booting();
        self::managing();

        static::deleting(function($project) {
            if ($project->isForceDeleting()) {
                foreach($project->files as $file) {
                    $file->delete();
                }
                Storage::disk('jobs')->deleteDirectory($project->id);
            }
        });
    }


    protected $appends = ['status'];

    protected $statuses = [
        'Ваша заявка розглядається',   //=0
        'Вітаю,Вас прийнято',   //=1
        'Нажаль,вашу заявку відхилено',   //=2
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    /**
     * Get string representation of status
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        return Arr::get($this->statuses, $this->status_id);
    }

}
