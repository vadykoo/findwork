<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Boot;
use Illuminate\Support\Facades\Storage;

class Support extends Model
{
    use SoftDeletes;
    use Boot;
    protected $fillable = ['description'];
    protected $dates =['deleted_at'];
    public static function boot() {
        parent::boot();
        self::booting();


        static::deleting(function($project) {
            if ($project->isForceDeleting()) {
                foreach($project->files as $file) {
                    $file->delete();
                }
                Storage::disk('supports')->deleteDirectory($project->id);
            }
        });
    }
}
