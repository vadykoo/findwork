<?php

namespace App\models;
use App\Traits\Boot;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
  //  use Selectable;
    use SoftDeletes;
    use Boot;
    protected $fillable = ['description'];
    protected $dates =['deleted_at'];

    public static function boot() {
        parent::boot();
        self::booting();


        static::deleting(function($project) {
            if ($project->isForceDeleting()) {
                foreach($project->files as $file) {
                    $file->delete();
                }
                Storage::disk('skills')->deleteDirectory($project->id);
            }
        });
    }

    public function skill(){
        return $this->hasMany(UserSkill::class);
    }
}
//trait Selectable
//{
//    public static function getSelectList($value = 'name', $key = 'id')
//    {
//        return static::latest()->owned()->pluck($value, $key);
//    }
//}