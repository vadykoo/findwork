<?php

namespace App\Models;

use App\Traits\Boot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Traits\Times;

class Job extends Model
{
     use Boot;
     use Times;

    protected $fillable = [
        'title',
        'city',
        'description',
        'start_date',
        'end_date',
        'price',
        'status'
    ];

    private static $minutes = ["хвилину", "хвилини", "хвилин"];
    private static $clock = ["годину", "години", "годин"];

    public function order(){
        return $this->hasMany(Order::class);
    }
    public function orderUSer(){
        return $this->hasMany(OrderUser::class);
    }


    public static function resTime($bool) {

        $user = Auth::guard('api')->user();

        if ($bool == true) {

        $order = Order::with('job')->where('user_id', $user->id)->get();
        $job = Job::whereNotIn('id', $order->pluck('job_id'))->get();

        $test = Job::pluck('created_at')->all();

        $result = self::memTime($test, self::$minutes, self::$clock);

        $i = 0;
        foreach ($job as $j) {
            $j = collect($j);
            $j->put('createdRecently', $result[$i]);
            $j->all();
            $all[] = $j;
            $i++;
        }


    }elseif($bool == false) {

                $arr_tags = DB::table('jobs')
            ->join('orders', 'job_id', '=', 'jobs.id')
            ->where('user_id', $user->id)
            ->orderBy('orders.status_id', 'desc')
            ->orderBy('orders.created_at', 'desc')
            ->get();

        $test = Job::whereIn('id', $arr_tags->pluck('job_id'))->pluck('created_at')->all();

         $result = self::memTime($test, self::$minutes, self::$clock);

        $i = 0;
        foreach ($arr_tags as $arr) {
            $arr = collect($arr);
            $arr->put('createdRecently', $result[$i]);
            $arr->all();
            $all[] = $arr;
            $i++;
        }
    }
        
         if (!empty($all) == false) $all = [];
        return $all;

    }


}
