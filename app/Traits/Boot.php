<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
trait Boot
{
    public static function booting()
    {
        static::updating(function ($table) {
            $table->updated_by = Auth::id();
        });

        static::creating(function ($table) {
            if (Auth::id()) {
                $table->created_by = Auth::id();
                $table->updated_by = Auth::id();

            }
        });
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy() {
        return $this->belongsTo(User::class, 'updated_by');
    }
}