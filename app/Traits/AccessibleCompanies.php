<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait AccessibleCompanies
{
    public function scopeAccessible($query)
    {
        switch (Auth::user()->role) {
            case 'administrator':
            case 'manager':
                return $query;
                break;
            case 'companyManager':
                return $query->where('managed_by', Auth::id());
                break;
        }
        return $query->where('id', 0);
    }
}