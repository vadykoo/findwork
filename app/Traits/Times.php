<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 26.04.2018
 * Time: 17:44
 */

namespace App\Traits;


trait Times
{

	    public static function getWord($number, $suffix) {

        $keys = array(2, 0, 1, 1, 1, 2);
        $mod = $number % 100;
        $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
        return $suffix[$suffix_key];

         }


}