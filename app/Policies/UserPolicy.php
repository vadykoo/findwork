<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the member.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $member
     * @return mixed
     */
    public function view(User $user, User $member)
    {
        //
    }

    /**
     * Determine whether the user can create members.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the member.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $member
     * @return mixed
     */
    public function update(User $user, User $member)
    {
        //
    }

    /**
     * Determine whether the user can delete the member.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $member
     * @return mixed
     */
    public function delete(User $user, User $member)
    {
        //
    }
}
