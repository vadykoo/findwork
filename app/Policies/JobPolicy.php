<?php

namespace App\Policies;

use App\Models\Job;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index(User $user)
    {
        //return collect(['manager', 'administrator'])->contains($user->role);
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return true;
                break;
        }
        return false;
    }

    public function view(User $user, Job $job)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return true;
                break;

        }
        return false;
    }
    public function create(User $user)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
        }
        return false;

    }
    public function show(User $user)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return true;
                break;
        }
        return false;

    }
    public function update(User $user)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
        }
        return false;
    }
    public function delete(User $user)
    {
        switch ($user->role) {
            case 'administrator':

                return true;
                break;
        }
        return false;
    }

}
