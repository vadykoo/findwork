<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Skill;
use Illuminate\Auth\Access\HandlesAuthorization;

class SkillPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the skill.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Skill  $skill
     * @return mixed
     */
    public function index(User $user)
    {
        //return collect(['manager', 'administrator'])->contains($user->role);
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return true;
                break;
        }
        return false;
    }

    public function view(User $user, Skill $skill)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return true;
                break;

        }
        return false;
    }

    /**
     * Determine whether the user can create skills.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return true;
                break;


        }
        return false;

    }

    /**
     * Determine whether the user can update the skill.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Skill  $skill
     * @return mixed
     */
    public function update(User $user, Skill $skill)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;

            case 'worker':
                return $skill->created_by == $user->id;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the skill.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Skill  $skill
     * @return mixed
     */
    public function delete(User $user, Skill $skill)
    {
        switch ($user->role) {
            case 'administrator':

                return true;
                break;
            case 'worker':
                return $skill->created_by == $user->id;
        }
        return false;
    }
}