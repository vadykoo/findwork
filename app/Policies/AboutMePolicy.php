<?php

namespace App\Policies;

use App\Models\User;
use App\Models\AboutMe;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class AboutMePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the aboutMe.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AboutMe  $aboutMe
     * @return mixed
     */

    public function index(User $user)
    {
        //return collect(['manager', 'administrator'])->contains($user->role);
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return true;
                break;

        }
        return false;
    }

    public function view(User $user, AboutMe $aboutMe)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return $aboutMe->created_by == $user->id;
                break;


        }
        return false;
    }

    /**
     * Determine whether the user can create aboutMes.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        switch ($user->role) {
            case 'administrator':
            case 'worker':
                return (empty(AboutMe::where('created_by', $user->id)->first()));
                break;
        }
    }

    /**
     * Determine whether the user can update the aboutMe.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AboutMe  $aboutMe
     * @return mixed
     */
    public function update(User $user, AboutMe $aboutMe)
    {
        switch ($user->role) {
            case 'administrator':
            case 'worker':
                return $aboutMe->created_by == $user->id;
                break;
        }
    }
    /**
     * Determine whether the user can delete the aboutMe.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\AboutMe  $aboutMe
     * @return mixed
     */
    public function delete(User $user, AboutMe $aboutMe)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
        }
    }
}
