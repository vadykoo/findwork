<?php

namespace App\Policies;

use App\Models\Member;
use App\Models\Project;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the county.
     *
     * @param  \App\Models\Member $user
     * @return mixed
     */
    public function index(Member $user)
    {
        //return collect(['manager', 'administrator'])->contains($user->role);
        switch ($user->role) {
            case 'administrator':
            case 'worker':
            return true;
                break;
        }
        return false;
    }


    /**
     * Determine whether the user can view the company.
     *
     * @param  \App\Models\Member $user
     * @param  \App\Models\Project $project
     * @return mixed
     */
    public function view(Member $user, Project $project)
    {
        switch ($user->role) {
            case 'administrator':
            case 'worker':
            return true;
                break;
//            case 'companyManager':
//                return $project->managed_by == $user->id;
//                break;
        }
        return false;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\Models\Member $user
     * @return mixed
     */
    public function create(Member $user)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
        }
        return false;
    }

    /**
     * Determine whether the user can update the company.
     *
     * @param  \App\Models\Member $user
     * @param  \App\Models\Project $project
     * @return mixed
     */
    public function update(Member $user, Project $project)
    {
        switch ($user->role) {
            case 'administrator':

                return true;
                break;

        }
        return false;
    }

    /**
     * Determine whether the user can delete the company.
     *
     * @param  \App\Models\Member $user
     * @param  \App\Models\Project $project
     * @return mixed
     */
    public function delete(Member $user, Project $project)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;

        }
        return false;
    }

}
