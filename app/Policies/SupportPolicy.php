<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Support;
use Illuminate\Auth\Access\HandlesAuthorization;

class SupportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the support.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Support  $support
     * @return mixed
     */

    public function index(User $user)
    {
        //return collect(['manager', 'administrator'])->contains($user->role);
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
        }
        return false;
    }

    public function view(User $user, Support $support)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return $support->created_by == $user->id;
                break;
        }
        return false;
    }

    /**
     * Determine whether the user can create supports.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        switch ($user->role) {
            case 'worker':
                return true;
                break;


        }
        return false;
    }

    /**
     * Determine whether the user can update the support.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Support  $support
     * @return mixed
     */
    public function update(User $user, Support $support)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
            case 'worker':
                return $support->created_by == $user->id;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the support.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Support  $support
     * @return mixed
     */
    public function delete(User $user, Support $support)
    {
        switch ($user->role) {
            case 'administrator':
                return true;
                break;
        }
        return false;
    }
}
