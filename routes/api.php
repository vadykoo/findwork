<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Auth::routes();

Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\RegisterController@register');

Route::group(['middleware' => 'auth:api'], function () {

    //User
    Route::apiResource('user', 'UserController');
    Route::get('tape', 'UserController@tape');

    //job

    Route::apiResource('jobs', 'JobController');
    Route::post('search', 'JobController@search');
    Route::get('myjobs', 'JobController@myjobs');
    Route::get('orderjobs', 'JobController@orderjobs');

    //    friends
    Route::get('friend', 'FriendController@myfriends');
    Route::post('friend/send', 'FriendController@sendFriendRequest');
    Route::get('friend/{user}/accept', 'FriendController@acceptFriendRequest');
    Route::get('friend/{user}/deny', 'FriendController@denyFriendRequest');
    Route::post('friend/delete', 'FriendController@destroyFriend');
    Route::get('friend/incoming', 'FriendController@incomingfriends');
    Route::get('friend/any', 'FriendController@anyfriends');
    Route::post('share','FriendController@addShare');

    //support
    Route::apiResource('support' , 'SupportController');
    Route::get('mysupport' ,  'SupportController@mysupport');

    //skill
    Route::apiResource('skill' , 'SkillController');
    Route::post('updatelevel/{userSkill}', 'SkillController@updatelevel');
    Route::get('myskill', 'SkillController@myskill');
    Route::post('deleteskill','SkillController@kick');


    //Password

    Route::post('updatepassword', 'Auth\ResetPasswordController@updatePassword');

//push

    Route::post('push', 'UserController@pushed');
});
Route::post('recovery', 'Auth\ForgotPasswordController@recovery');
Route::post('sendcode', 'Auth\ForgotPasswordController@sendcode');
Route::post('resetpass', 'Auth\ForgotPasswordController@resetpass');
Route::post('pushmes', 'PushController@sendMessage');