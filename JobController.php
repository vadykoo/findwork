<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobRequest;
use App\Models\Job;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
class JobController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Job::class);
    }

    public function getWord($number, $suffix) {
        $keys = array(2, 0, 1, 1, 1, 2);
        $mod = $number % 100;
        $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
        return $suffix[$suffix_key];
    }



    public function index()
    {



        $user = Auth::guard('api')->user();
        $order = Order::with('job')->where('user_id', $user->id)->get();
        $job = Job::whereNotIn('id', $order->pluck('job_id'))->get();

        $test = Job::pluck('created_at')->all();




        $x = 0;
        foreach ($test as $key => $value) {

            $jobData = $value->format('Y-m-d');
            $dataJ[] = explode('-', $jobData);


            $data = date("Y-m-d");
            $dataT[] = explode('-', $data);

            if ($dataJ[$x][2] == $dataT[$x][2]) {

                $date = date("H:i:s");

                $tes = strtotime($date) - strtotime($value->format('H:i:s'));

                if($x == 2) {
//                    $resData[] = date('H:i:s', $tes);
//                    dd($resData);
                }


                $resData[] = date('H:i:s', $tes);

                $arrData[] = explode(':', $resData[$x]);

//                dd($arrData);

                if ($arrData[$x][0] == 00) {

                    $array = array("хвилину", "хвилини", "хвилин");

                    $result[] = $arrData[$x][1] .' '. self::getWord($arrData[$x][1], $array) .' тому';

                } else {
                    $array = array("годину", "години", "годин");
                    $cut = substr($arrData[$x][0], 1);
                    $result[] = $cut .' '. self::getWord($cut, $array) .' тому';
                }
            } else {
                $result[] = NULL;
            }
            $x++;
        }

        $i = 0;
        foreach ($job as $j) {
            $j = collect($j);
            $j->put('createdRecently', $result[$i]);
            $j->all();
            $all[] = $j;
            $i++;
        }

                if (!empty($all) == false) $all = [];


        return response()->json($all, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\JobRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        $job = Job::create($request->all());
        return response()->json($job, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        return response()->json($job, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\JobRequest $request
     * @param  \App\Models\Job $job
     * @return \Illuminate\Http\Response
     */
    public function update(Job $job, JobRequest $request)
    {
        $job->update($request->all());
        return response()->json($job, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job $job
     * @return \Illuminate\Http\Response
     * @throws
     */
    public function destroy(Job $job)
    {
        $job->delete();
        return response()->json(null, 204);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function seeone(Request $request)
    {
        $seeProject = Job::where('id', $request->id)->first();

        if(!empty($_COOKIE['count'])) {
            $cookcount = explode(".", $_COOKIE['count']);

            $i = 0;
            foreach($cookcount as $it) {
                if($it[$i] == $request->id) $true = true;
                $i++;
            }

            if($request != true) {
                $addsee = $seeProject->count + 1;
                $seeProject->count = $addsee;
                $seeProject->save();
                Cookie::forever('count', $request->id);
            }

        } else {
            $addsee = $seeProject->count + 1;
            $seeProject->count = $addsee;
            $seeProject->save();
            setcookie('count', $request->id);
        }

        return response()->json($seeProject, 200);
    }
    public function search(Request $request) {

        if(!empty($request->search)) {
            $allsearch = Job::where('title', 'like', '%' . $request->search . '%')->get();
            return response()->json($allsearch, 400);
        } else {
            return response()->json("Search field is empty!", 400);
        }

    }
    public function order(Request $request)
    {
        $user = Auth::guard('api')->user();

        $uJob = Order::where([
            ['user_id', '=', $user->id],
            ['job_id', '=', $request->job_id],])->first();
        if(!empty($uJob) == false) {
            $data = array(
                'user_id' => $user->id,
                'job_id' => $request->job_id,
            );

            $jobs = new Order();
            $jobs->create($data);

            return response()->json($data, 201);
        }
        return response()->json("Error", 201);

    }
    public function myjobs() {
        $user = Auth::guard('api')->user();
        $order = Order::with('job')->where('user_id', $user->id)->orderBy('id', 'desc')->get();
        return response()->json($order, 201);
    }

    public function kickorder(Request $requests) {
        $select = Order::where('id', $requests->id)->first();
        if(!empty($select)) {
            $order = Order::where('id', $requests->id)->delete();
            return response()->json($order, 201);
        } else {
            return response()->json("record not found", 201);
        }
    }
}